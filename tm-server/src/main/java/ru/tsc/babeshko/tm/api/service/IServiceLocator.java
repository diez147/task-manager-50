package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IUserServiceDTO;

public interface IServiceLocator {

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    IUserServiceDTO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}