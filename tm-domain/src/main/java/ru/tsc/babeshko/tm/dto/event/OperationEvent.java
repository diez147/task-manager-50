package ru.tsc.babeshko.tm.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.enumerated.EntityOperationType;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    @NotNull
    private EntityOperationType operationType;

    @NotNull
    private Object entity;

    @Nullable
    private String table;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final Date date = new Date();

    public OperationEvent(@NotNull final EntityOperationType operationType, @NotNull final Object entity) {
        this.operationType = operationType;
        this.entity = entity;
    }

}
